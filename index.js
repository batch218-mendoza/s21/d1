
//List of studentID's of all graduating students of the class
console.log("Variable vs Array");

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";


console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);
							
let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927",];
console.log(studentNumbers)

console.log("--------------")
// [SECTION] Arrays
/*
       - Arrays are used to store multiple related values in a single variable.
       - They are declared using square brackets ([]) also known as "Array Literals"
       - Arrays it also provides access to a number of functions/methods that help in manipulation array.
           - Methods are used to manipulate information stored within the same object.
       - Array are also objects which is another data type.
       - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
       - Syntax:
           let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
   */

console.log("Examples of Array: ");

// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90]; //arrays could store numeric values
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "RedFox", "Gateway", "toshiba", "fujitsu"];

console.log(grades);
console.log(computerBrands);

//Possible use of an array but it is not recommended
let mixedArr = ["John", "Doe", 12, false, null, undefined, {}];

console.log(mixedArr);



let myTasks = [
"drink html",
"eat javascript",
"inhale css",
"bake mongoDB"
];


console.log("-----------------")

let city1 = "tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// [SECTION] .length property
// ".length" property allows us to get the set the total number of items in an array
console.log("Using .length property");
console.log("Length / size of myTasks array:" + myTasks.length);
console.log("Length / size of cities array:" + cities.length);

console.log("------------");

console.log("Using .length property for string size: ");

let fullName = " Jan Jonell Mendoza";
console.log("length/size of fullName array: " + fullName.length);

console.log("Removing the last element from an array");
myTasks.length = myTasks.length -1;
console.log(myTasks.length);
console.log(myTasks);


// to delete a specific item an array we can employ array methods "pop method"

cities.length--;
console.log(cities);


// we can't do the same on string
fullName.length = fullName.length -1;
console.log(fullName.length);
console.log(fullName);


// we can also add the length of an array.
console.log("Add an element to an array");
let theBeatles = ["John", "Paul", "Ringo", "george"];
console.log(theBeatles);

theBeatles[4] = " cardo";
console.log(theBeatles);

let removedBeatles = theBeatles.pop();
console.log(removedBeatles);


console.log("-----------------");

// [SECTION] Reading from Arrays
   /*
       - Accessing array elements is one of the common task that we do with an array.
       - This can be done through the use of array indexes.
       - Each element in an array is associated with it's own index number.
       - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
       - Array indexes it is actually refer to a memory address/location

       Array Address: 0x7ffe942bad0
       Array[0] = 0x7ffe942bad0
       Array[1] = 0x7ffe942bad4
       Array[2] = 0x7ffe942bad8

       - Syntax
           arrayName[index];
   */


// let grades = [98.5, 94.3, 89.2, 90];
console.log(grades[0]); // The Index number of the first element is zero

//let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "RedFox", "Gateway", "toshiba", "fujitsu"];
console.log(computerBrands[3]);

console.log("---------")
let lakersLegends = ["kobe", "Shaq", "lebron", "magic", "Kareem"];


console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

console.log(lakersLegends[1]+ " & " + lakersLegends[3]);

console.log("-----------------");
console.log("Reassigning an element from an array");
	// You can also reassign array values using indeces

	console.log("Array before Reassingment");
	console.log(lakersLegends);

	lakersLegends[2] = "Gasol";
	console.log("Array after Reassingment");
	console.log(lakersLegends);


console.log("------------");


console.log("access the last element of an array");
let bullLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullLegends.length-1;

console.log(bullLegends[lastElementIndex]);

console.log("---------");

console.log("Adding a new items into an array using indeces");
//adding items into an array
// we can add items in array using indeces


const newArr = [];
console.log(newArr[0]); // undefined

newArr[0] = "cloud Stripe";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockheart";
console.log(newArr);

console.log("---------");

console.log("add element in the end of an array using '.length' property");

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

console.log("------------");
console.log("Displaying the content of an array 1 by 1");
/*
console.log(newArr[0]);
console.log(newArr[1]);
console.log(newArr[2]);*/

for(let i = 0; i < newArr.length; i++){
	console.log(newArr[i]);
}

console.log("---------------");
console.log("filtering an array using loop and conditional statements: ");

let numArr2 = [5, 12, 30, 46, 40, 52];

for(let index=0; index < numArr2.length; index++){
	if (numArr2[index] % 5==0){
		console.log(numArr2[index] + " is divisible by 5.");
	}

	else{
		console.log(numArr2[index] + " is not divisible by 5.");
	}
}


// [SECTION] Multidimensional Arrays
/*
   -Multidimensional arrays are useful for storing complex data structures.
   - A practical application of this is to help visualize/create real world objects.
   - This is frequently used to store data for mathematic computations, image processing, and record management.
   - Array within an Array
*/

// Create chessboaard


let chessBoard = [
   ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
   ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
   ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
   ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
   ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
   ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
   ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
   ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];


console.table(chessBoard);

//access a element of a multidimensional arrays
//Syntax: multiarr[outerarr][innerArray]
					//col     //row


console.log(chessBoard[3][4]); //e4

console.log("Pawn moves to: " + chessBoard[2][5]);